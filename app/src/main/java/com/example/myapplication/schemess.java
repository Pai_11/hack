package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.view.View;
import android.widget.TextView;


public class schemess extends AppCompatActivity {

    TextView quote;

    public Button but1;

    public void init()
    {
        but1= (Button)findViewById(R.id.but1);
        but1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent toy=new Intent(schemess.this, search.class);

                startActivity(toy);
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schemess);

        quote= (TextView) findViewById(R.id.quote1);



        final Button button1=findViewById(R.id.button1);
        final Button button2=findViewById(R.id.button2);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quote.setText("Pick a Ministry-");
                Spinner mySpinner1 = (Spinner) findViewById(R.id.spinner2);
                ArrayAdapter<String> myAdapter1 = new ArrayAdapter<String>(schemess.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.names1));
                myAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mySpinner1.setAdapter(myAdapter1);
                button2.setEnabled(false);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quote.setText("Pick a State-");
                Spinner mySpinner1= (Spinner) findViewById(R.id.spinner2);
                ArrayAdapter<String> myAdapter1 = new ArrayAdapter<String>(schemess.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.names3));
                myAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mySpinner1.setAdapter(myAdapter1);
                button1.setEnabled(false);

            }
        });

        init();

        Spinner mySpinner3= (Spinner) findViewById(R.id.spinner5);
        ArrayAdapter<String> myAdapter3 = new ArrayAdapter<String>(schemess.this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.names4));
        myAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner3.setAdapter(myAdapter3);

    }
}
